package com.epam.andriistakhiv.exercise1.feline;

import com.epam.andriistakhiv.exercise1.Pet;
import com.epam.andriistakhiv.exercise1.animals.Feline;

public class Cat extends Feline implements Pet {
    public void eat() {
        System.out.println("Cat is eating");
    }

    public void makeNoise() {
        System.out.println("Meow");
    }

    public void beFriendly() {
        System.out.println("Friendly cat");
    }
}
