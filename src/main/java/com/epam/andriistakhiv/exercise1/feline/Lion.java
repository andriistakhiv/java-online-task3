package com.epam.andriistakhiv.exercise1.feline;

import com.epam.andriistakhiv.exercise1.HuntingForPrey;
import com.epam.andriistakhiv.exercise1.animals.Feline;

public class Lion extends Feline implements HuntingForPrey {
    public void eat() {
        System.out.println("Lion is eating");
    }

    public void makeNoise() {
        System.out.println("Lion is making noise");
    }

    public void huntingForPrey() {
        System.out.println("Hunting for prey like lion");
    }
}
