package com.epam.andriistakhiv.exercise1;

public abstract class Animal {
    public abstract void eat();

    public abstract void makeNoise();

    public void roam() {
        System.out.println("Animal is roaming");
    }

    public void sleep() {
        System.out.println("Animal is sleeping");
    }
}
