package com.epam.andriistakhiv.exercise1.canine;

import com.epam.andriistakhiv.exercise1.Pet;
import com.epam.andriistakhiv.exercise1.animals.Canine;

public class Dog extends Canine implements Pet {
    public void eat() {
        System.out.println("Dog is eating");
    }

    public void makeNoise() {
        System.out.println("Woof woof");
    }

    public void beFriendly() {
        System.out.println("Good boy");
    }
}
