package com.epam.andriistakhiv.exercise1.canine;

import com.epam.andriistakhiv.exercise1.HuntingForPrey;
import com.epam.andriistakhiv.exercise1.animals.Canine;

public class Wolf extends Canine implements HuntingForPrey {
    public void eat() {
        System.out.println("Wolf is eating");
    }

    public void makeNoise() {
        System.out.println("Wolf is making noise");
    }

    public void huntingForPrey() {
        System.out.println("Hunting for prey like wolf");
    }
}
