package com.epam.andriistakhiv.exercise1;

import com.epam.andriistakhiv.exercise1.birds.Eagle;
import com.epam.andriistakhiv.exercise1.feline.Cat;
import com.epam.andriistakhiv.exercise1.fish.Shark;

public class Main {
    public static void main(String[] args) {
        Main main = new Main();

        main.poly(new Cat());
        main.poly(new Shark());
        main.poly(new Eagle());
    }

    private void poly(Animal a) {
        a.eat();
        a.makeNoise();
        a.roam();
        a.sleep();
    }
}
