package com.epam.andriistakhiv.exercise1.fish;

import com.epam.andriistakhiv.exercise1.Pet;
import com.epam.andriistakhiv.exercise1.animals.Fish;

public class GoldenFish extends Fish implements Pet {
    public void eat() {
        System.out.println("Golden fish is eating");
    }

    public void makeNoise() {
        System.out.println("bul bul");
    }

    public void beFriendly() {
        System.out.println("Chill out in the aquarium");
    }
}
