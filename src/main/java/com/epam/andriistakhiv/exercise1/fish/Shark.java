package com.epam.andriistakhiv.exercise1.fish;

import com.epam.andriistakhiv.exercise1.HuntingForPrey;
import com.epam.andriistakhiv.exercise1.animals.Fish;

public class Shark extends Fish implements HuntingForPrey {
    public void eat() {
        System.out.println("Shakr is eating");
    }

    public void makeNoise() {
        System.out.println("Shark is making noise");
    }

    public void huntingForPrey() {
        System.out.println("Hunting for prey like shark");
    }
}
