package com.epam.andriistakhiv.exercise1.animals;

import com.epam.andriistakhiv.exercise1.Animal;

public abstract class Bird extends Animal {
    public void fly() {
        System.out.println("Bird is flying");
    }
}
