package com.epam.andriistakhiv.exercise1.animals;

import com.epam.andriistakhiv.exercise1.Animal;

public abstract class Fish extends Animal {
    public void swim() {
        System.out.println("Fish is swimming");
    }
}
