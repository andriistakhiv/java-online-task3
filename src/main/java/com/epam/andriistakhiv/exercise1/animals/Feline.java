package com.epam.andriistakhiv.exercise1.animals;

import com.epam.andriistakhiv.exercise1.Animal;

public abstract class Feline extends Animal {
    public void roam() {
        System.out.println("Roaming like feline");
    }
}
