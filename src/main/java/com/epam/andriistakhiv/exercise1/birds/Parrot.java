package com.epam.andriistakhiv.exercise1.birds;

import com.epam.andriistakhiv.exercise1.animals.Bird;

public class Parrot extends Bird {
    public void eat() {
        System.out.println("Parrot is eating plants");
    }

    public void makeNoise() {
        System.out.println("Parrot is making noise");
    }
}
