package com.epam.andriistakhiv.exercise1.birds;

import com.epam.andriistakhiv.exercise1.HuntingForPrey;
import com.epam.andriistakhiv.exercise1.animals.Bird;

public class Eagle extends Bird implements HuntingForPrey {
    public void eat() {
        System.out.println("Eagle is eating meat");
    }

    public void makeNoise() {
        System.out.println("Eagle is making noise");
    }

    public void huntingForPrey() {
        System.out.println("Hunting for prey like eagle");
    }
}
