package com.epam.andriistakhiv.exercise2.shapes;

import com.epam.andriistakhiv.exercise2.Shape;

public class Square extends Shape {
    public Square(String color) {
        super(color, "Square");
    }
}
