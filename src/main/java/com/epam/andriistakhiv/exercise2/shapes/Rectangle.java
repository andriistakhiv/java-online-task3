package com.epam.andriistakhiv.exercise2.shapes;

import com.epam.andriistakhiv.exercise2.Shape;

public class Rectangle extends Shape {
    public Rectangle(String color) {
        super(color, "Rectangle");
    }
}
