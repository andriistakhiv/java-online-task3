package com.epam.andriistakhiv.exercise2.shapes;

import com.epam.andriistakhiv.exercise2.Shape;

public class Circle extends Shape {
    public Circle(String color) {
        super(color, "Circle");
    }
}
