package com.epam.andriistakhiv.exercise2.shapes;

import com.epam.andriistakhiv.exercise2.Shape;

public class Triangle extends Shape {
    public Triangle(String color) {
        super(color, "Triangle");
    }
}
