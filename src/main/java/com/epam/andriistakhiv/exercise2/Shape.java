package com.epam.andriistakhiv.exercise2;

public abstract class Shape {
    String color;
    String shape;

    public Shape(String color, String shape) {
        this.color = color;
        this.shape = shape;

        System.out.println("This is " + color + " " + shape);
    }
}
